from rest_framework.generics import GenericAPIView
from rest_framework.mixins import UpdateModelMixin
from rest_framework.response import Response


class FilterByUserView(GenericAPIView):

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)


class PatchOnlyUpdateView(UpdateModelMixin):

    def update(self, request, *args, **kwargs):
        if request.method == 'PATCH':
            return super().update(request, *args, **kwargs)
        else:
            data = 'PUT is not allowed'
            status = 405
            return Response(data=data, status=status)
