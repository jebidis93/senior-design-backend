from django.urls import path

from senior_design_backend.calendar.views import (
    GetActivitiesView,
    GetActivityByIdView,
    GetCategoriesView,
    GetCategoryByIdView,
    GetEventsView,
    GetEventByIdView
)

app_name = "calendar"
urlpatterns = [
    path("categories", view=GetCategoriesView.as_view(), name="categories"),
    path("categories/<id>", view=GetCategoryByIdView.as_view(), name="categories"),
    path("activities", view=GetActivitiesView.as_view(), name="activities"),
    path("activities/<id>", view=GetActivityByIdView.as_view(), name="activities"),
    path("events", view=GetEventsView.as_view(), name="events"),
    path("events/<id>", view=GetEventByIdView.as_view(), name="events"),
]
