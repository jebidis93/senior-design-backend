import pytest
from unittest import mock
from django.utils import timezone
from datetime import timedelta

from senior_design_backend.calendar.models import Event, ToDoEvent
from senior_design_backend.users.models import User

pytestmark = pytest.mark.django_db

NOW = timezone.now()
ONE_LATER = NOW + timedelta(1)


class TestToDoEvent:
 
    @pytest.mark.parametrize('approximate_time,minimum_time,start_time,end_input,calls', [
                            (timedelta(1), timedelta(1), NOW, None, 1),
                            (timedelta(1), timedelta(1), NOW, ONE_LATER, 1),
                            (timedelta(1), timedelta(0), NOW, ONE_LATER, 1),
                            (timedelta(2), timedelta(0), NOW, ONE_LATER, 1),
                            (timedelta(0), timedelta(1), NOW, ONE_LATER, 1),
                            (timedelta(0), timedelta(0), NOW, ONE_LATER, 1),
                            (timedelta(1), timedelta(0), NOW, NOW, 1),
                            (timedelta(1), timedelta(1), NOW, NOW, 0),
                            (timedelta(0), timedelta(1), NOW, NOW, 0),
                            (timedelta(0), timedelta(2), NOW, ONE_LATER, 0)
                            ])
    def test_attempt_event_add(self, approximate_time, minimum_time, start_time, end_input, calls):
        # this tests each of the main set of if/elif for all
        # possible inputs that result in an Event being made

        user = User.objects.create(username='test')
        user.set_password('test')
        user.save()

        with mock.patch('senior_design_backend.calendar.models.Event.objects.create') as mock_create:
            tde = ToDoEvent(title='test', summary='',
                            user=user, minimum_time=minimum_time,
                            approximate_time=approximate_time)

            tde.attempt_event_add(start_time, end_input)
            assert mock_create.call_count == calls

    @pytest.mark.parametrize('existing', [
                            True,
                            False
                            ])
    def test_add_event_before_first_existing_event(self, existing):
        # test add_event_before_first_existing_event adds an event
        # when there are no existing events or when there is
        # an existing event

        user = User.objects.create(username='test')
        user.set_password('test')
        user.save()

        if existing:
            start = NOW + timedelta(10000)
            end = ONE_LATER + timedelta(10000)
            Event.objects.create(title=' ', user=user, start=start, end=end)

        with mock.patch('senior_design_backend.calendar.models.ToDoEvent.attempt_event_add') as mock_add:
            tde = ToDoEvent.objects.create(title='test', summary='',
                                           user=user, minimum_time=timedelta(1),
                                           approximate_time=timedelta(1))
            mock_add.reset_mock()
            tde.add_event_before_first_existing_event()
            assert mock_add.call_count == 1

    @pytest.mark.parametrize('can_add', [
                            True,
                            False
                            ])
    def test_add_events_in_middle(self, can_add):
        # test add_events_in_middle adds an events in middle
        # if possible, if not, does not add events

        user = User.objects.create(username='test')
        user.set_password('test')
        user.save()

        modifier = 0
        if can_add:
            modifier = 1
        start_one = NOW
        end_one = ONE_LATER
        start_two = ONE_LATER + timedelta(modifier)
        end_two = ONE_LATER + timedelta(1 + modifier)
        Event.objects.create(title=' ', user=user, start=start_one, end=end_one)
        Event.objects.create(title=' ', user=user, start=start_two, end=end_two)

        with mock.patch('senior_design_backend.calendar.models.Event.objects.create') as mock_create:

            tde = ToDoEvent(title='test', summary='',
                            user=user, minimum_time=timedelta(1),
                            approximate_time=timedelta(1))
            tde.events_list = list(Event.objects.filter(user=user).order_by('start'))
            tde.add_events_in_middle()
            assert mock_create.call_count == can_add
