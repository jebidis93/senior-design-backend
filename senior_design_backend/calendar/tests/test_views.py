import datetime
import pytest

from django.conf import settings
from django.test import Client, RequestFactory
from django.utils import timezone

from senior_design_backend.calendar.models import Event
from senior_design_backend.users.models import User
from senior_design_backend.users.views import UserRedirectView, UserUpdateView
from senior_design_backend.users.tests.factories import UserFactory

pytestmark = pytest.mark.django_db


class TestBadStatusCodes:
    

    @pytest.mark.parametrize('route', [
                            '/api/events',
                            '/api/events/1'
                            ])
    def test_user_not_signed_in(self, route):
        client = Client()
        request = client.get(route)
        assert request.status_code ==403

    @pytest.mark.parametrize('route', [
                            '/api/events/1'
                            ])
    def test_user_accessing_other_user_routes(self, route):
        pass

    @pytest.mark.parametrize('route,status_code', [
                            ('/api/events', 200),
                            ('/api/events/{}', 200),
                            ])
    def test_events_get(self, route, status_code):
        user = User.objects.create(username='test')
        user.set_password('test')
        user.save()
        start = timezone.now()
        end = start + datetime.timedelta(1)
        event = Event.objects.create(user=user, title='test',start=start, end=end)
        client = Client()
        client.login(username='test', password='test')
        route = route.format(event.id)
        request = client.get(route)
        assert request.status_code == status_code

    @pytest.mark.parametrize('route,status_code', [
                            ('/api/events', 201),
                            ('/api/events/1', 405)
                            ])
    def test_post(self, route, status_code):
        user = User.objects.create(username='test')
        user.set_password('test')
        user.save()
        client = Client()
        client.login(username='test', password='test')
        data = {'title': 'post_test', 'start':'2020-04-10T18:00:00Z', 'end':'2020-04-10T19:00:00Z', 'user':user.id}
        request = client.post(route, data=data)
        assert request.status_code == status_code

    @pytest.mark.parametrize('route', [
                            '/api/events',
                            ])
    def test_delete(self, route):
        user = User.objects.create(username='test')
        user.set_password('test')
        user.save()
        client = Client()
        client.login(username='test', password='test')
        request = client.delete(route)
        assert request.status_code == 405
