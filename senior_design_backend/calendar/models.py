from datetime import datetime, timedelta
from pytz import UTC
from django.db.models import (CASCADE, CharField, DateTimeField,
                              DurationField, ForeignKey)
from django.utils import timezone
from model_utils.models import TimeStampedModel
from senior_design_backend.users.models import User


class Category(TimeStampedModel):

    name = CharField(max_length=50)


class Activity(TimeStampedModel):

    name = CharField(max_length=50)
    category = ForeignKey(Category, on_delete=CASCADE, related_name='activities')
    user = ForeignKey(User, on_delete=CASCADE)


class ToDoEvent(TimeStampedModel):

    title = CharField(max_length=50)
    user = ForeignKey(User, on_delete=CASCADE)
    summary = CharField(null=True, max_length=200)
    approximate_time = DurationField()
    minimum_time = DurationField()

    def attempt_event_add(self, start_time, end_input=None):
        approximate_time = self.approximate_time
        minimum_time = self.minimum_time
        time_difference = timedelta()
        end_time = None

        if end_input:
            time_difference = end_input - start_time

        if not end_input:
            end_time = start_time + approximate_time

        elif approximate_time >= time_difference >= minimum_time:
            end_time = end_input

        elif approximate_time < time_difference >= minimum_time:
            end_time = start_time + minimum_time

        if end_time:
            new_event = Event.objects.create(title=self.title,
                                             summary=self.summary,
                                             start=start_time,
                                             end=end_time,
                                             user=self.user,
                                             to_do_event=self
                                             )
            self.approximate_time -= new_event.duration
            return new_event

    def add_event_before_first_existing_event(self):
        events = self.events_list
        minimum_time = self.minimum_time
        now = timezone.now()
        start_time = now - (datetime.min.replace(tzinfo=UTC) - now) % minimum_time
        end_time = None

        if events:
            first_event = events[0]
            end_time = first_event.start

        new_event = self.attempt_event_add(start_time, end_time)

        if new_event:
            self.events_list = [new_event] + events

    def add_events_in_middle(self):
        approximate_time = self.approximate_time
        events = self.events_list
        event_index = 0
        new_events_list = []

        while events and event_index < len(events) - 1 and approximate_time > timedelta():
            first_event = events[event_index]
            second_event = events[event_index + 1]
            event_index += 1
            new_event = self.attempt_event_add(first_event.end, second_event.start)

            new_events_list.append(first_event)
            if new_event:
                new_events_list.append(new_event)

        self.events_list = new_events_list + [events[-1]]

    def add_event_after_final_existing_event(self):
        last_event = self.events_list[-1]
        new_event = self.attempt_event_add(last_event.end)
        if new_event:
            self.events_list.append(new_event)

    def add_new_events(self):

        self.add_event_before_first_existing_event()
        self.add_events_in_middle()
        self.add_event_after_final_existing_event()

    def save(self, *args, **kwargs):

        super().save()
        now = timezone.now()
        self.events_list = list(self.user.event_set.filter(end__gt=now).order_by('start'))
        self.add_new_events()


class Event(TimeStampedModel):

    title = CharField(max_length=50)
    user = ForeignKey(User, on_delete=CASCADE)
    summary = CharField(null=True, max_length=200)
    start = DateTimeField()
    end = DateTimeField()
    location = CharField(max_length=50, null=True)
    activity = ForeignKey(Activity, on_delete=CASCADE, null=True, related_name='events')
    to_do_event = ForeignKey(ToDoEvent, on_delete=CASCADE, null=True)

    @property
    def duration(self):
        return self.end - self.start

    def save(self, *args, **kwargs):
        if self.start >= self.end:
            raise ValueError("Start time must be before end time")
        print('new event')
        super().save(*args, **kwargs)
