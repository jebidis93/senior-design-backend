from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class CalendarConfig(AppConfig):
    name = "senior_design_backend.calendar"
    verbose_name = _("Calendar")

    def ready(self):
        pass
