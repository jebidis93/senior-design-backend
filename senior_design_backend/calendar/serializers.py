from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from senior_design_backend.calendar.models import Activity, Category, Event


class CategorySerializer(ModelSerializer):
    activities = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ('id', 'name', 'created', 'modified', 'activities')
        read_only_fields = ('modified', 'created', 'activities')


class ActivitySerializer(ModelSerializer):
    events = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Activity
        fields = ('id', 'name', 'category', 'created', 'modified', 'events')
        read_only_fields = ('modified', 'created', 'events')


class EventSerializer(ModelSerializer):

    class Meta:
        model = Event
        fields = ('id', 'title', 'summary', 'start', 'end', 'user', 'created', 'modified', 'to_do_event')
        read_only_fields = ('user', 'modified', 'created', 'to_do_task')
