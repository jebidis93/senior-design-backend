from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAdminUser

from senior_design_backend.base_classes.views import FilterByUserView
from senior_design_backend.calendar.models import Activity, Category, Event
from senior_design_backend.calendar.serializers import ActivitySerializer, CategorySerializer, EventSerializer


class AdminOnlyMethods(IsAdminUser):

    def has_permission(self, request, view):
        print(request.method)
        if request.method == 'GET':
            return True
        else:
            return super().has_permission(request, view)


class GetCategoriesView(ListCreateAPIView):

    permission_classes = (AdminOnlyMethods,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class GetCategoryByIdView(RetrieveUpdateDestroyAPIView):

    permission_classes = (AdminOnlyMethods,)
    lookup_field = "id"
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class GetActivitiesView(ListCreateAPIView):

    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer


class GetActivityByIdView(RetrieveUpdateDestroyAPIView):

    lookup_field = "id"
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer


class GetEventsView(ListCreateAPIView, FilterByUserView):

    queryset = Event.objects.all()
    serializer_class = EventSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GetEventByIdView(RetrieveUpdateDestroyAPIView, FilterByUserView):

    lookup_field = "id"
    queryset = Event.objects.all()
    serializer_class = EventSerializer

    def perform_update(self, serializer):

        user = self.request.user
        event_id = self.kwargs.get('id')
        event = Event.objects.get(pk=event_id)
        if event.user == user:
            super().perform_update(serializer)
