from django.urls import path

from senior_design_backend.users.views import (
    AllUsersView,
    user_redirect_view,
    user_update_view,
    user_detail_view,
)

app_name = "users"
urlpatterns = [
    path("~redirect/", view=user_redirect_view, name="redirect"),
    path("~update/", view=user_update_view, name="update"),
    path("<str:username>/", view=user_detail_view, name="detail"),
    path("all", view=AllUsersView.as_view(), name='all')
]
