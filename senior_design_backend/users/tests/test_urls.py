import pytest
from django.conf import settings
from django.urls import reverse, resolve

pytestmark = pytest.mark.django_db


def test_detail(user: settings.AUTH_USER_MODEL):
    assert (
        reverse("users:detail", kwargs={"username": user.username})
        == f"/auth/{user.username}/"
    )
    assert resolve(f"/auth/{user.username}/").view_name == "users:detail"


def test_update():
    assert reverse("users:update") == "/auth/~update/"
    assert resolve("/auth/~update/").view_name == "users:update"


def test_redirect():
    assert reverse("users:redirect") == "/auth/~redirect/"
    assert resolve("/auth/~redirect/").view_name == "users:redirect"
