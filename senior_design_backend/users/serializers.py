from rest_framework.serializers import ModelSerializer

from senior_design_backend.users.models import User


class AllUsersSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ["username", "id"]
